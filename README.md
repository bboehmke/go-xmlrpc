# XML-RPC Library

[![godoc](https://godoc.org/gitlab.com/bboehmke/go-xmlrpc?status.svg)](https://godoc.org/gitlab.com/bboehmke/go-xmlrpc)

This is a simple [XML-RPC](https://en.wikipedia.org/wiki/XML-RPC) client implementation.
There is also a basic server part that can be used for callbacks.

## Usage

Import via

```
gitlab.com/bboehmke/go-xmlrpc
```

then create a client and do a RPC call to a server:

```go
// create client
client, err := xmlrpc.NewClient("https://mxl-rpc.server")

// send request
response, err := client.Call("system.listMethods")
```

For the detailed usage see the [API doc](https://godoc.org/gitlab.com/bboehmke/go-xmlrpc).
