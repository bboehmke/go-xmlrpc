package xmlrpc

import "net/http"

// Option defines a function that configures a Client by mutating it
type Option func(client *client)

// HttpClient option allows setting custom HTTP Client
func HttpClient(httpClient *http.Client) Option {
	return func(c *client) {
		c.client = httpClient
	}
}
